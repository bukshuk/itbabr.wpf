﻿using System.Windows.Input;

namespace ITBabr.WPF.Command
{
	public interface IBindCommand : ICommand
	{
		void Update();
	}
}
