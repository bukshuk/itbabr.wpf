﻿using System;
using System.Windows;

using ITBabr.WPF.Observable;

namespace ITBabr.WPF.Command
{
	public abstract class BindCommandBase : ObservableObject, IBindCommand
	{
		public event EventHandler CanExecuteChanged;

		public string Text
		{
			get => _text;
			protected set => Set(ref _text, value.ToUpper());
		}
		private string _text;

		public Visibility Visibility
		{
			get => _visible;
			private set => Set(ref _visible, value);
		}
		private Visibility _visible;

		protected Func<Visibility> VisibilityDelegate { get; set; }

		public BindCommandBase()
		{
			UpdateVisibility();
		}

		public abstract void Execute(object parameter);

		public virtual bool CanExecute(object parameter)
		{
			return Visibility == Visibility.Visible;
		}

		public virtual void Update()
		{
			UpdateVisibility();

			CanExecuteChanged?.Invoke(this, null);
		}

		private void UpdateVisibility()
		{
			if (VisibilityDelegate != null)
			{
				Visibility = VisibilityDelegate.Invoke();
			}
		}
	}
}
