﻿using System;
using System.Windows;

namespace ITBabr.WPF.Command
{
	public class BindCommand : BindCommandBase
	{
		public BindCommand(Action execute, Func<bool> canExecute, Func<Visibility> visibilityDelegate)
		{
			ExecuteDelegate = execute ?? throw new ArgumentNullException();
			CanExecuteDelegate = canExecute ?? (() => true);
			VisibilityDelegate = visibilityDelegate;
		}

		public BindCommand(Action execute, Func<bool> canExecute)
			: this(execute, canExecute, null)
		{
		}

		public BindCommand(Action execute, Func<Visibility> visibilityDelegate)
			: this(execute, null, visibilityDelegate)
		{
		}

		public BindCommand(Action execute)
			: this(execute, null, null)
		{
		}

		public override void Execute(object parameter)
		{
			ExecuteDelegate.Invoke();
		}

		public override bool CanExecute(object parameter)
		{
			return base.CanExecute(parameter) && CanExecuteDelegate.Invoke();
		}

		private Action ExecuteDelegate { get; }

		protected Func<bool> CanExecuteDelegate { get; set; }
	}
}
