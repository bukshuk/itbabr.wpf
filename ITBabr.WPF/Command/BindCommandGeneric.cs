﻿using System;
using System.Windows;

namespace ITBabr.WPF.Command
{
	public class BindCommand<T> : BindCommandBase
	{
		public BindCommand(Action<T> execute, Func<T, bool> canExecute, Func<Visibility> visibilityDelegate)
		{
			ExecuteDelegate = execute ?? throw new ArgumentNullException();
			CanExecuteDelegate = canExecute ?? (p => true);
			VisibilityDelegate = visibilityDelegate;
		}

		public BindCommand(Action<T> execute, Func<T, bool> canExecute)
			: this(execute, canExecute, null)
		{
		}

		public BindCommand(Action<T> execute, Func<Visibility> visibilityDelegate)
			: this(execute, null, visibilityDelegate)
		{
		}

		public BindCommand(Action<T> execute)
			: this(execute, null, null)
		{
		}

		public override void Execute(object parameter)
		{
			if (parameter is T typedParameter)
			{
				ExecuteDelegate.Invoke(typedParameter);
			}
		}

		public override bool CanExecute(object parameter)
		{
			bool canExecute = false;

			if (parameter is T typedParameter)
			{
				canExecute = base.CanExecute(parameter) && CanExecuteDelegate.Invoke(typedParameter);
			}

			return canExecute;
		}

		private Action<T> ExecuteDelegate { get; }

		protected Func<T, bool> CanExecuteDelegate { get; set; }
	}
}
