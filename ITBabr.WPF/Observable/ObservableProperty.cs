﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace ITBabr.WPF.Observable
{
	public class ObservableProperty<T> : ObservableObject, IObservable<T>
	{
		public T Value
		{
			get => _value;
			set
			{
				if (Set(ref _value, value))
				{
					SetValueAction?.Invoke(_value);
				}
			}
		}
		private T _value;

		public ObservableProperty(int debounceMilliseconds = 0, Func<IObservable<T>, IObservable<T>> observablePredicate = null)
		{
			CurrentObservable = System.Reactive.Linq.Observable.FromEvent<T>(h => SetValueAction += h, h => SetValueAction -= h);

			if (debounceMilliseconds > 0)
			{
				CurrentObservable = CurrentObservable.Throttle(TimeSpan.FromMilliseconds(debounceMilliseconds));
			}

			if (observablePredicate != null)
			{
				CurrentObservable = observablePredicate(CurrentObservable);
			}

			CurrentObservable = CurrentObservable
				.DistinctUntilChanged()
				.ObserveOn(DispatcherScheduler.Current);
		}

		public IDisposable Subscribe(IObserver<T> observer)
		{
			return CurrentObservable.Subscribe(observer);
		}

		private IObservable<T> CurrentObservable { get; }

		private Action<T> SetValueAction;
	}
}
