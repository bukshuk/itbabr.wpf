﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace ITBabr.WPF.Observable
{
	public abstract class ObservableObject : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual bool Set<T>(ref T field, T newValue, [CallerMemberName] string propertyName = null)
		{
			bool setRequired = !EqualityComparer<T>.Default.Equals(field, newValue);

			if (setRequired)
			{
				field = newValue;

				RaisePropertyChanged(propertyName);
			}

			return setRequired;
		}

		protected void RaisePropertyChanged([CallerMemberName]string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		protected void RaisePropertyChanged<T>(Expression<Func<T>> propertyExpression)
		{
			RaisePropertyChanged(GetPropertyName(propertyExpression));
		}

		protected static string GetPropertyName<T>(Expression<Func<T>> propertyExpression)
		{
			if (!(propertyExpression.Body is MemberExpression memberExpression))
			{
				var unaryExpression = propertyExpression.Body as UnaryExpression ?? throw new NotImplementedException();

				memberExpression = unaryExpression.Operand as MemberExpression ?? throw new NotImplementedException();
			}

			return memberExpression.Member.Name;
		}
	}
}
