﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace ITBabr.WPF.Observable
{
	public abstract class ValidatedObject : ObservableObject, INotifyDataErrorInfo
	{
		public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public virtual bool HasErrors => ErrorsByPropertyName.Any();

        public Func<string, IEnumerable<string>> PropertyErrorsCallback { private get; set; } = e => Enumerable.Empty<string>();

        public IEnumerable GetErrors(string propertyName)
        {
            return ErrorsByPropertyName.ContainsKey(propertyName) ? ErrorsByPropertyName[propertyName] : null;
        }

        protected override bool Set<T>(ref T field, T newValue, [CallerMemberName] string propertyName = null)
        {
            bool setRequired = base.Set(ref field, newValue, propertyName);

            if (setRequired)
            {
                ValidateProperty(propertyName, newValue);
            }

            return setRequired;
        }

        protected virtual IEnumerable<string> GetPropertyErrors(string propertyName)
        {
            yield break;
        }

        protected void ValidateProperty<T>(Expression<Func<T>> propertyExpression)
        {
            T propertyValue = propertyExpression.Compile()();

            ValidateProperty(GetPropertyName(propertyExpression), propertyValue);
        }

        private void ValidateProperty<T>(string propertyName, T value)
        {
            ClearErrors(propertyName);

            ValidatePropertyAnnotated(propertyName, value);

            ValidatedPropertyCustom(propertyName);

            OnErrorChanged(propertyName);
        }

        private void ValidatePropertyAnnotated<T>(string propertyName, T value)
        {
            var validationContext = new ValidationContext(this) { MemberName = propertyName };
            var validationResults = new List<ValidationResult>();

            Validator.TryValidateProperty(value, validationContext, validationResults);

            foreach (var result in validationResults)
            {
                AddError(propertyName, result.ErrorMessage);
            }
        }

        private void ValidatedPropertyCustom(string propertyName)
        {
            foreach (string error in GetPropertyErrors(propertyName))
            {
                AddError(propertyName, error);
            }

            foreach (string error in PropertyErrorsCallback(propertyName))
            {
                AddError(propertyName, error);
            }
        }

        private void AddError(string propertyName, string error)
        {
            if (!ErrorsByPropertyName.ContainsKey(propertyName))
            {
                ErrorsByPropertyName[propertyName] = new List<string>();
            }
            if (!ErrorsByPropertyName[propertyName].Contains(error))
            {
                ErrorsByPropertyName[propertyName].Add(error);
            }
        }

        private void ClearErrors(string propertyName)
        {
            if (ErrorsByPropertyName.ContainsKey(propertyName))
            {
                ErrorsByPropertyName.Remove(propertyName);
            }
        }

        private void OnErrorChanged(string propertyName)
        {
            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));

            RaisePropertyChanged(() => HasErrors);
        }

        private Dictionary<string, List<string>> ErrorsByPropertyName { get; } = new Dictionary<string, List<string>>();
    }
}
